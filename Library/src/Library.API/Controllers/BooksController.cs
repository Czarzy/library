﻿using AutoMapper;
using Library.API.Entities;
using Library.API.Helpers;
using Library.API.Models;
using Library.API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.API.Controllers
{
    [Route("api/authors/{authorId}/books")]
    public class BooksController: Controller
    {
        private ILibraryRepository _repository;
        private ILogger<BooksController> _logger;
        private IUrlHelper _urlHelper;

        public BooksController(ILibraryRepository repository, ILogger<BooksController> logger, IUrlHelper urlHelper)
        {
            _repository = repository;
            _logger = logger;
            _urlHelper = urlHelper;
        }

        [HttpGet(Name ="GetBooksForAuthor")]
        public IActionResult GetBooks(Guid authorId)
        {
            if(!_repository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var booksFromDb = _repository.GetBooksForAuthor(authorId);

            var books = Mapper.Map<IEnumerable<BookDto>>(booksFromDb);

            books = books.Select(book =>
            {
                book = CreateLinksForBook(book);
                return book;
            });

            var wrapper = new LinkedCollectionResourceWrapperDto<BookDto>(books);

            return Ok(CreateLinksForBooks(wrapper));
        }

        [HttpGet("{id}", Name = "GetBookForAuthor")]
        public IActionResult GetBookById(Guid authorId, Guid id)
        {
            if (!_repository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookFromDb = _repository.GetBookForAuthor(authorId, id);
            if (bookFromDb == null)
                return NotFound();

            var book = Mapper.Map<BookDto>(bookFromDb);

            return Ok(CreateLinksForBook(book));
        }

        [HttpPost(Name ="CreateBookForAuthor")]
        public IActionResult CreateBook(Guid authorId, [FromBody]BookForCreationDto book)
        {
            if(book == null)
            {
                return BadRequest();
            }

            if(book.Description == book.Title)
            {
                ModelState.AddModelError(nameof(BookForCreationDto), "Description and title must be different.");
            }

            if(!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if(!_repository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookEntity = Mapper.Map<Book>(book);

            _repository.AddBookForAuthor(authorId, bookEntity);

            if (!_repository.Save())
            {
                throw new Exception("There was problem with handling your request.");
            }

            var bookToReturn = Mapper.Map<BookDto>(bookEntity);

            return CreatedAtRoute("GetBookForAuthor", new { authorId = authorId, id = bookToReturn.Id }, CreateLinksForBook(bookToReturn));
        }

        [HttpDelete("{id}", Name ="DeleteBookForAuthor")]
        public IActionResult DeleteBookForAuthor(Guid authorId, Guid id)
        {
            if(!_repository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookFromRepo = _repository.GetBookForAuthor(authorId, id);

            if(bookFromRepo == null)
            {
                return NotFound();
            }

            _repository.DeleteBook(bookFromRepo);

            if(!_repository.Save())
            {
                throw new Exception($"Deleting book {bookFromRepo} failed.");
            }

            _logger.LogInformation(100, $"Book {id} for {authorId} was deleted.");

            return NoContent();
        }

        [HttpPut("{id}", Name ="UpdateBookForAuthor")]
        public IActionResult UpdateBookForAuthor(Guid authorId, Guid id, [FromBody] BookForUpdateDto book)
        {
            if (book == null)
            {
                return NotFound();
            }

            if (book.Description == book.Title)
            {
                ModelState.AddModelError(nameof(BookForCreationDto), "Description and title must be different.");
            }

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            if (!_repository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookFromRepo = _repository.GetBookForAuthor(authorId, id);

            if (bookFromRepo == null)
            {
                //if book doesn't exist, create it
                var bookToAdd = Mapper.Map<Book>(book);
                bookToAdd.Id = id;
                _repository.AddBookForAuthor(authorId, bookToAdd);

                if(!_repository.Save())
                {
                    throw new Exception("Failed to upsert book!");
                }

                var bookToReturn = Mapper.Map<BookDto>(bookToAdd);

                return CreatedAtRoute("GetBookForAuthor", new { authorId = authorId, id = bookToReturn.Id }, CreateLinksForBook(bookToReturn));
            }

            Mapper.Map(book, bookFromRepo);

            _repository.UpdateBookForAuthor(bookFromRepo);

            if(!_repository.Save())
            {
                throw new Exception("Couldn't update resource.");
            }

            return NoContent();
        }

        [HttpPatch("{id}", Name ="PartiallyUpdateBookForAuthor")]
        public IActionResult PartiallyUpdateBook(Guid authorId, Guid id, [FromBody] JsonPatchDocument<BookForUpdateDto> patchDoc)
        {
            if(patchDoc == null)
            {
                return BadRequest();
            }

            if (!_repository.AuthorExists(authorId))
            {
                return NotFound();
            }

            var bookFromRepo = _repository.GetBookForAuthor(authorId, id);

            if (bookFromRepo == null)
            {
                //if there's no book to patch create new book
                var bookDto = new BookForUpdateDto();
                patchDoc.ApplyTo(bookDto, ModelState);

                if (bookDto.Description == bookDto.Title)
                {
                    ModelState.AddModelError(nameof(BookForUpdateDto), "Description and title must be different.");
                }

                TryValidateModel(bookDto);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                var bookToAdd = Mapper.Map<Book>(bookDto);
                bookToAdd.Id = id;

                _repository.AddBookForAuthor(authorId, bookToAdd);

                if(!_repository.Save())
                {
                    throw new Exception("Couldn't upsert new book using patch.");
                }

                var bookToReturn = Mapper.Map<BookDto>(bookToAdd);
                return CreatedAtRoute("GetBookForAuthor", new { authorId = authorId, id = bookToReturn.Id }, CreateLinksForBook(bookToReturn));
            }

            var bookToPatch = Mapper.Map<BookForUpdateDto>(bookFromRepo);

            patchDoc.ApplyTo(bookToPatch, ModelState);

            if (bookToPatch.Description == bookToPatch.Title)
            {
                ModelState.AddModelError(nameof(BookForCreationDto), "Description and title must be different.");
            }

            TryValidateModel(bookToPatch);

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            Mapper.Map(bookToPatch, bookFromRepo);

            _repository.UpdateBookForAuthor(bookFromRepo);

            if(!_repository.Save())
            {
                throw new Exception("Failed to patch the book");
            }

            return NoContent();
        }

        private BookDto CreateLinksForBook(BookDto book)
        {
            book.Links.Add(new LinkDto(_urlHelper.Link("GetBookForAuthor", new { id = book.Id }), "self", "GET"));

            book.Links.Add(new LinkDto(_urlHelper.Link("DeleteBookForAuthor", new { id = book.Id }), "delete_book", "DELETE"));

            book.Links.Add(new LinkDto(_urlHelper.Link("UpdateBookForAuthor", new { id = book.Id }), "update_book", "PUT"));

            book.Links.Add(new LinkDto(_urlHelper.Link("PartiallyUpdateBookForAuthor", new { id = book.Id }), "partially_update_book", "PATCH"));

            return book;
        }

        private LinkedCollectionResourceWrapperDto<BookDto> CreateLinksForBooks(LinkedCollectionResourceWrapperDto<BookDto> booksWrapper)
        {
            booksWrapper.Links.Add(new LinkDto(_urlHelper.Link("GetBooksForAuthor", new { }),
                "self", "GET"));

            return booksWrapper;
        }
    }
}
