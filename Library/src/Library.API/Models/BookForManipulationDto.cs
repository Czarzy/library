﻿using System.ComponentModel.DataAnnotations;

namespace Library.API.Models
{
    //base class for all book class manipulation such as update
    public abstract class BookForManipulationDto
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(500)]
        public virtual string Description { get; set; }
    }
}
