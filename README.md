# README #

Welcome to my Library.API project. This is just an RESTful api that allows to store data about books and authors.

This api is made in ASP.NET Core 2.0. I've made it for educational purposes only. It strenghten my ASP.NET Core knowledge
about creating RESTful Api. So there's some more advanced REST things like HATEOAS, Caching, and so on.